#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int javatime_to_timestamp(time_t javatime, char* trgbuf, int bufsize)
{
    struct tm* info;
    time_t milli = (javatime % 1000);
    time_t epoch = javatime/1000;

    if (!trgbuf || bufsize<1)
        return 0;

    info = localtime(&epoch);
    // + 1 on month
    snprintf(trgbuf, bufsize, "%.4d-%.2d-%.2d %.2d:%.2d:%.2d.%d", 1900 + info->tm_year, (info->tm_mon+1), info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec,  (int)milli);
    return 1;
}


int main(int argc, char *argv[])
{
    #define bufsz 256
    char timestamp[bufsz];
    time_t rawtime = (time_t)1616518277550; // example java time value

    if(argc == 2) {
        rawtime = (time_t)strtoul(argv[1], NULL, 10); //add various checks...
    }

    if(! javatime_to_timestamp(rawtime, timestamp, bufsz)) {
      fprintf(stderr, "something bad happened\n");
      return 1;
    }
    printf("%s\n", timestamp);
    return 0;
}
